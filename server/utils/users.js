class Users {
  constructor () {
    this.users = []
  }

  addUser (id, name, room) {
    let user = {id, name, room}
    this.users.push(user)
    return user
  }
  removeUser (id) {
    let user = this.getUser(id)
    if (user) {
      this.users = this.users.filter(user => user.id !== id)
    }
    return user
  }
  getUser (id) {
    return this.users.filter(user => user.id === id)[0]
  }

  checkDuplicateName (name) {
    return this.users.filter(user => user.name === name).length === 0
  }

  checkDuplicateRoomName (room) {
    let roomNames = this.users.map(user => user.room.toLowerCase())
    let roomIndex = roomNames.indexOf(room.toLowerCase())
    if ( roomIndex >= 0) return this.users[roomIndex].room === room
    return true
  }

  getUserList (room) {
    let users = this.users.filter(user => user.room === room)
    let namesArray = users.map(user => user.name)
    return namesArray
  }

  getChatRoomList () {
    let rooms = this.users.map(user => user.room)
    let uniqueRooms = rooms.reduce((a, b) => {
      if (a.indexOf(b) < 0) a.push(b)
      return a
    }, [])
    return uniqueRooms
  }
}

module.exports = Users
