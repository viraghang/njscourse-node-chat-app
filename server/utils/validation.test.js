/* global describe,it */
const expect = require('expect')
const { isRealString, isTooLong } = require('./validation')

describe('isRealString', () => {
  it('should reject non-string values', () => {
    let str = 5789
    let res = isRealString(str)
    expect(res).toBe(false)
  })
  it('should reject string with only spaces', () => {
    let str = '     '
    let res = isRealString(str)
    expect(res).toBe(false)
  })
  it('should allow string with non-space characters', () => {
    let str = ';.,][-=]'
    let res = isRealString(str)
    expect(res).toBe(true)
  })
})

describe('isTooLong', () => {
  it('should reject too long string', () => {
    let str = 'fjkldsakdfjlkglk dkjf gjkdf'
    let res = isTooLong(str)
    expect(res).toBe(false)
  })
  it('should accept 20 or less char strings', () => {
    let str = 'Toyota Yaris'
    let res = isTooLong(str)
    expect(res).toBe(true)
  })
})
