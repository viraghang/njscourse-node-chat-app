/* global it,describe */

const expect = require('expect')

const { generateMessage, generateLocationMessage } = require('./message')

describe('generateMessage', () => {
  it('should generate the correct message object', () => {
    let from = 'josef@russia.com'
    let text = 'Play the arpeggio'
    let response = generateMessage(from, text)
    expect(response.from).toBe(from)
    expect(response.text).toBe(text)
    expect(response).toInclude({from, text})
    expect(response.createdAt).toBeA('number')
  })
})

describe('generateLocationMessage', () => {
  it('should generate correct location object', () => {
    let from = 'Batman'
    let latitude = 47.505079699999996
    let longitude = 19.0557586
    let locationMsg = generateLocationMessage(from, latitude, longitude)
    expect(locationMsg).toInclude({
      from,
      url: `https://www.google.com/maps?q=${latitude},${longitude}`
    })
    expect(locationMsg.createdAt).toBeA('number')
  })
})
