let isRealString = str => typeof str === 'string' && str.trim().length > 0
let isTooLong = str => str.length <= 20

module.exports = { isRealString, isTooLong }
