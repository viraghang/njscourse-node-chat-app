/* global describe,it,beforeEach */
const expect = require('expect')

const Users = require('./users')

describe('Users class', () => {

  let users

  beforeEach(() => {
    users = new Users()
    users.users = [{
      id: '1',
      name: 'Mike',
      room: 'Node Course'
    }, {
      id: '2',
      name: 'Jen',
      room: 'React Course'
    }, {
      id: '3',
      name: 'Julie',
      room: 'Node Course'
    }]
  })

  it('should add new user', () => {
    let users = new Users()
    let user = {
      id: '123',
      name: 'Zoli',
      room: 'Office Fans'
    }
    users.addUser(user.id, user.name, user.room)

    expect(users.users).toEqual([user])
  })

  it('should return names for node course', () => {
    let userList = users.getUserList('Node Course')
    expect(userList).toEqual(['Mike', 'Julie'])
  })

  it('should return names for react course', () => {
    let userList = users.getUserList('React Course')
    expect(userList).toEqual(['Jen'])
  })

  it('should remove a user', () => {
    let toBeRemoved = users.users[0]
    let removal = users.removeUser(toBeRemoved.id)
    expect(users.users.length).toBe(2)
    expect(removal).toEqual(toBeRemoved)
  })

  it('should not remove a user', () => {
    let removal = users.removeUser('4')
    expect(users.users.length).toBe(3)
    expect(removal).toBe(undefined)
  })

  it('should find user', () => {
    let query = users.getUser('1')
    expect(query).toEqual(users.users[0])
  })

  it('should not find user', () => {
    let query = users.getUser('4')
    expect(query).toBe(undefined)
  })

  it('should reject user name', () => {
    let task = users.checkDuplicateName('Mike')
    expect(task).toBe(false)
  })

  it('should accept user name', () => {
    let task = users.checkDuplicateName('Nancy')
    expect(task).toBe(true)
  })

  it('should reject room name', () => {
    let task = users.checkDuplicateRoomName('nOde coUrse')
    expect(task).toBe(false)
  })

  it('should accept room name', () => {
    let task = users.checkDuplicateRoomName('Node Course')
    expect(task).toBe(true)
  })
})
