require('./config/config.js')

const http = require('http')
const express = require('express')
const path = require('path')
const socketIO = require('socket.io')

const { generateMessage, generateLocationMessage } = require('./utils/message')
const { isRealString, isTooLong } = require('./utils/validation')
const Users = require('./utils/users')

let app = express()
let server = http.createServer(app)
let io = socketIO(server)
let users = new Users()

app.use(express.static(path.join(__dirname, '../public')))

io.on('connection', socket => {
  socket.on('join', (params, callback) => {
    if (!isRealString(params.name) || !isRealString(params.room)) {
      return callback('Name and room name are required')
    } else if (!isTooLong(params.name) || !isTooLong(params.room)) {
      return callback('Either the name or the room name is too long, keep it not longer than 20 chars')
    } else if (!users.checkDuplicateName(params.name) || !users.checkDuplicateRoomName(params.room)) {
      return callback('Name or room name already exists')
    }
    socket.join(params.room)
    users.removeUser(socket.id)
    users.addUser(socket.id, params.name, params.room)

    io.to(params.room).emit('updateUserList', users.getUserList(params.room))
    io.emit('updateChatRoomList', users.getChatRoomList())

    socket.emit('newMessage', generateMessage('Admin', 'Welcome to the chat app'))
    socket.broadcast.to(params.room).emit('newMessage', generateMessage('Admin', `${params.name} has joined.`))

    callback()

    // socket.leave('The Office Fans');

    // io.emit -> io.to('The Office Fans').emit
    // socket.broadcast.emit -> socket.broadcast.to('The Office Fans').emit
    // socket.emit
  })

  socket.on('createLocationMessage', (coords) => {
    let user = users.getUser(socket.id)
    if (user) {
      io.to(user.room).emit('newLocationMessage', generateLocationMessage(user.name, coords.latitude, coords.longitude))
    }
  })

  socket.on('disconnect', () => {
    let user = users.removeUser(socket.id)
    if (user) {
      io.to(user.room).emit('updateUserList', users.getUserList(user.room))
      io.to(user.room).emit('newMessage', generateMessage('Admin', `${user.name} has left`))
    }
  })

  socket.on('createMessage', (msg, callback) => {
    let user = users.getUser(socket.id)
    if (user && isRealString(msg.text)) {
      io.to(user.room).emit('newMessage', generateMessage(user.name, msg.text))
    }
    callback()
  })
})

server.listen(process.env.PORT, () => console.log(`Server is listening on ${process.env.PORT}`))

// Chatrooms case insensitive
// Usernames should be unique
// List of the currently active chatrooms, input box on the join page
