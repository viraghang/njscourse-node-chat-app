/* global io,$,alert,moment,Mustache,Notification,Audio */

// Ask for notification permission
if (window.Notification && Notification.permission !== 'denied') Notification.requestPermission()

// Preparing sound
let notificationSound = new Audio('/sound/coins.mp3')

// Watching focus
let isFocused = true
document.addEventListener('focusin', function (e) { isFocused = true })
document.addEventListener('focusout', function (e) { isFocused = false })

var socket = io()

function scrollToBottom () {
  // Selectors
  var messages = $('#messages')
  var newMessage = messages.children('li:last-child')

  // Heights
  var clientHeight = messages.prop('clientHeight')
  var scrollTop = messages.prop('scrollTop')
  var scrollHeight = messages.prop('scrollHeight')
  var newMessageHeight = newMessage.innerHeight()
  var lastMessageHeight = newMessage.prev().innerHeight()

  if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
    messages.scrollTop(scrollHeight)
  }
}

function notify (from, body) {
  if (window.Notification && Notification.permission === 'granted' && !isFocused) {
    let notification = new Notification(from, {body})
    notificationSound.play()
    notification.close()
  }
}

socket.on('connect', function () {
  var params = $.deparam(window.location.search)

  socket.emit('join', params, function (err) {
    if (err) {
      alert(err)
      window.location.href = '/'
    }
  })
})

socket.on('disconnect', function () {
  console.log('Disconnected from server')
})

socket.on('updateUserList', function (users) {
  var ol = $('<ol></ol>')
  users.forEach(function (user) {
    ol.append($('<li></li>').text(user))
  })
  $('#users').html(ol)
})

socket.on('updateChatRoomList', function (rooms) {
  var ol = $('<ol></ol>')
  rooms.forEach(function (room) {
    ol.append($('<li></li>').text(room))
  })
  $('#chatrooms').html(ol)
})

socket.on('newMessage', function (msg) {
  var formattedTime = moment(msg.createdAt).format('h:mm a')
  var template = $('#message-template').html()
  var html = Mustache.render(template, {
    from: msg.from,
    text: msg.text,
    createdAt: formattedTime
  })

  $('#messages').append(html)
  notify(msg.from, msg.text)
  scrollToBottom()
  // $('#messages').append('<li>' + msg.from + '\n' + msg.text + '</li>')
  // var li = $('<li></li>')
  // li.text(`${formattedTime} --- ${msg.from}: ${msg.text}`)
  // $('#messages').append(li)
})

socket.on('newLocationMessage', function (msg) {
  var formattedTime = moment(msg.createdAt).format('h:mm a')
  var template = $('#location-message-template').html()
  var html = Mustache.render(template, {
    from: msg.from,
    url: msg.url,
    createdAt: formattedTime
  })

  // var a = $('<a></a>').text('My current location').attr({target: '_blank', href: msg.url})
  // var li = $('<li></li>').text(`${formattedTime} --- ${msg.from}: `).append(a)
  $('#messages').append(html)
  scrollToBottom()
})

$('#message-form').on('submit', function (e) {
  e.preventDefault()

  socket.emit('createMessage', {
    text: $('[name="message"]').val()
  }, function () {
    $('[name="message"]').val('')
  })
})

var locationButton = $('#send-location')

locationButton.on('click', function () {
  locationButton.prop('disabled', true).text('Sending Location...')
  if (!navigator.geolocation) {
    return alert('Geolocation not supported by your browser')
  }
  navigator.geolocation.getCurrentPosition(function (position) {
    socket.emit('createLocationMessage', {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    })
    locationButton.prop('disabled', false).text('Send Location')
  }, function () {
    alert('Unable to fetch location')
  })
})
